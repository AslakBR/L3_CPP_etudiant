#ifndef LISTE_H
#define LISTE_H

struct Noeud {
	int _valeur;
	Noeud * _suivant;
};

struct Liste {
	Noeud * _tete;
	Liste();
	
	void ajouterDevant(int valeur);
	int getTaille() const;
	int getElement(int indice) const;
	
	~Liste();
};
#endif // LISTE_H
