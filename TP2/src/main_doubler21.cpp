#include "Doubler.hpp"

#include <iostream>

int main() {
    std::cout << doubler(21) << std::endl;
    std::cout << doubler(21.f) << std::endl;
    
    int a=42;
    int *p=&a;
    *p=37;
    
    int *t=new int[10];
    *(t+2)=42;
    std::cout<<a<<std::endl<<*(t+2)<<std::endl;
    delete [] t;
    t=nullptr;
    
    return 0;
}

