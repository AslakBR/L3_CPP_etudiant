#include "Liste.h"
#include <iostream>

Liste::Liste(){
	_tete=nullptr;
}

void Liste::ajouterDevant(int valeur){
	Noeud *nouveau=new Noeud();
	nouveau->_valeur=valeur;
	nouveau->_suivant=_tete;
	_tete=nouveau;
	nouveau=nullptr;
}

int Liste::getTaille() const{
	int cpt=0;
	Noeud * parcours;
	parcours=_tete;
	while(parcours!=nullptr){
		cpt++;
		parcours=parcours->_suivant;
	}
	return cpt;
}

int Liste::getElement(int indice) const{
	int cpt=0;
	Noeud * parcours;
	parcours=_tete;
	while(cpt!=indice){
		cpt++;
		parcours=parcours->_suivant;
	}
	if(parcours!=nullptr && cpt==indice){
			return parcours->_valeur;
	}
	return -1;
}

Liste::~Liste(){
	if(_tete==nullptr)
		return;
	Noeud *success=_tete->_suivant;
	delete _tete;
	_tete=success;
	while(_tete!=nullptr){
		success=success->_suivant;
		delete _tete;
		_tete=success;
	}
}
