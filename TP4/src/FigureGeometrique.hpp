#ifndef FIGUREGEOMETRIQUE_HPP
#define FIGUREGEOMETRIQUE_HPP

#include "Point.hpp"
#include "Couleur.hpp"

class FigureGeometrique {
	private:
	Couleur _couleur;
	
	public:
	FigureGeometrique(const Couleur &couleur);
	const Couleur& getCouleur() const;
	virtual void afficher() const;
	virtual ~FigureGeometrique();
	};
#endif //figuregeometrique.hpp
