#include <iostream>
#include "Ligne.hpp"
#include "PolygoneRegulier.hpp"
#include <vector>

int main(){

	Couleur yes=Couleur{0,1,0};
	Point p1=Point{3,2};
	Point p2=Point{4,1};
	Ligne *l=new Ligne(yes, p1, p2);
	//l->afficher();

	PolygoneRegulier *r= new PolygoneRegulier(yes,Point{100,200},50,5);
	//r->afficher();
	
	std::vector<FigureGeometrique*> test1{r,l};
	for(FigureGeometrique *i: test1){
		i->afficher();
	}
	return 0;
}
