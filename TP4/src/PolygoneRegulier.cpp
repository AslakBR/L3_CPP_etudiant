#include <iostream>
#include "PolygoneRegulier.hpp"
#include <cmath>
#include <math.h>

	PolygoneRegulier::PolygoneRegulier(const Couleur &couleur,const Point &centre,int rayon,int nbCotes):
			FigureGeometrique(couleur),_nbPoints(nbCotes),_points(new Point[_nbPoints])
		{
			float degreTot=180;
			int cpt=0;
			for(int i=0;i<360;i+=360/nbCotes){
				Point P;
				P._x=(centre._x+rayon*cos(i*M_PI/degreTot));
				P._y=(centre._y+rayon*sin(i*M_PI/degreTot));
				*(_points+cpt++)=P;
			}
		};
	
	void PolygoneRegulier::afficher() const{
		std::cout<<"Polygone Regulier "<<getCouleur()._r<<"_"<<getCouleur()._g<<"_"<<getCouleur()._b<<" ";
		for(int i=0;i<_nbPoints;i++){
				std::cout<<(_points+i)->_x<<"_"<<(_points+i)->_y<<" ";
			}
		std::cout<<std::endl;
	}
	
	int PolygoneRegulier::getNbPoints() const{
		return _nbPoints;
	}
	
	const Point & PolygoneRegulier::getPoint(int indice) const{
		return *(_points+indice);
	}

	PolygoneRegulier::~PolygoneRegulier(){
		delete [] _points;
	}
