#include "PolygoneRegulier.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupPoly) { };

TEST(GroupPoly, Poly_test1)  {
	Couleur yes=Couleur{0,1,0};
	PolygoneRegulier r=PolygoneRegulier(yes,Point{100,200},50,5);	
    CHECK_EQUAL(r.getNbPoints(),5);
}

TEST(GroupPoly, Poly_test2)  {
	Couleur yes=Couleur{0,1,0};
	PolygoneRegulier r=PolygoneRegulier(yes,Point{100,200},50,5);	
    CHECK_EQUAL(r.getPoint(2)._x, 59);
    CHECK_EQUAL(r.getPoint(2)._y, 229);
}

