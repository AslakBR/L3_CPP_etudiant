#include <iostream>
#include "Location.hpp"
#include "Client.hpp"
#include "Produit.hpp"
#include "Magasin.hpp"

int main(){

	Location Test;
	Test.id_Client=0;
	Test.id_Produit=2;
	Test.afficherLocation();
	std::cout<<std::endl<<std::endl;
	Client TestCli(42, "toto");
	TestCli.afficherClient();
	std::cout<<std::endl<<std::endl;
	
	Produit TestPro(18, "C'est un beau produit");
	TestPro.afficherProduit();
	std::cout<<std::endl<<std::endl;
	
	Magasin TestMag;
	TestMag.ajouterClient("Brunet");
	TestMag.ajouterClient("Cordier");
	TestMag.ajouterClient("Decarpentry");
	TestMag.ajouterClient("Deleglise");
	
	TestMag.ajouterProduit("Pas ouf");
	TestMag.ajouterProduit("Déjà vu");
	TestMag.ajouterProduit("LIS");
	TestMag.ajouterProduit("Tales of");
	TestMag.ajouterProduit("Magic");
	
	TestMag.afficherClients();
	std::cout<<std::endl<<std::endl;
	TestMag.afficherProduits();
	
	TestMag.ajouterLocation(1,4);
	TestMag.ajouterLocation(3,3);
	TestMag.ajouterLocation(2,5);
	
	std::cout<<std::endl<<std::endl;
	
	TestMag.afficherLocations();
	
	return 0;
}
