#include <iostream>
#include <string>
#include "Client.hpp"

	Client::Client(int _id, std::string const & _nom):
		_id(_id), _nom(_nom)
	{}
	
	int Client::getId() const{
		return _id;
	}
	
	const std::string & Client::getNom() const{
		return _nom;
	}
	
	void Client::afficherClient() const{
		std::cout<<"Client ("<<_id<<", "<<_nom<<")"<<std::endl;
	}
