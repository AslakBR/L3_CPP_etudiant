#include "Magasin.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupMagasin) { };

TEST(GroupMagasin, Magasin_test1)  {
	Magasin TestMag;
	TestMag.ajouterClient("Brunet");
	TestMag.ajouterClient("Cordier");
	TestMag.ajouterClient("Decarpentry");
	TestMag.ajouterClient("Deleglise");

    CHECK_EQUAL(TestMag.nbClients(), 4);
}

TEST(GroupMagasin, Magasin_test2)  {
	
	Magasin TestMag;
	TestMag.ajouterClient("Brunet");
	TestMag.ajouterClient("Cordier");
	TestMag.ajouterClient("Decarpentry");
	TestMag.ajouterClient("Deleglise");
	
	TestMag.supprimerClient(2);
	
    CHECK_EQUAL(TestMag.nbClients(), 3);
}

TEST(GroupMagasin, Magasin_test3)  {
	Magasin TestMag;	
	TestMag.ajouterProduit("Pas ouf");
	TestMag.ajouterProduit("Déjà vu");
	TestMag.ajouterProduit("LIS");
	TestMag.ajouterProduit("Tales of");
	TestMag.ajouterProduit("Magic");
	
    CHECK_EQUAL(TestMag.nbProduits(), 5);
}
	
TEST(GroupMagasin, Magasin_test4)  {
	Magasin TestMag;	
	TestMag.ajouterProduit("Pas ouf");
	TestMag.ajouterProduit("Déjà vu");
	TestMag.ajouterProduit("LIS");
	TestMag.ajouterProduit("Tales of");
	TestMag.ajouterProduit("Magic");
	
	TestMag.supprimerProduit(1);
	TestMag.supprimerProduit(5);
	TestMag.supprimerProduit(2);
	
    CHECK_EQUAL(TestMag.nbProduits(), 2);
}

TEST(GroupMagasin, Magasin_test5)  {
	Magasin TestMag;	
	TestMag.ajouterProduit("Pas ouf");
	TestMag.ajouterProduit("Déjà vu");
	TestMag.ajouterProduit("LIS");
	TestMag.ajouterProduit("Tales of");
	TestMag.ajouterProduit("Magic");
	
	TestMag.supprimerProduit(1);
	TestMag.supprimerProduit(5);
	TestMag.supprimerProduit(28);
	
    CHECK_EQUAL(TestMag.nbProduits(), 3);
}

TEST(GroupMagasin, Magasin_test6)  {
	Magasin TestMag;
	TestMag.ajouterClient("Brunet");
	TestMag.ajouterClient("Cordier");
	TestMag.ajouterClient("Decarpentry");
	TestMag.ajouterClient("Deleglise");
	
	TestMag.supprimerClient(2);
	TestMag.supprimerClient(12);
	
    CHECK_EQUAL(TestMag.nbClients(), 3);
}

TEST(GroupMagasin, Magasin_test7)  {
	Magasin TestMag;
	TestMag.ajouterClient("Brunet");
	TestMag.ajouterClient("Cordier");
	TestMag.ajouterClient("Decarpentry");
	TestMag.ajouterClient("Deleglise");

	TestMag.ajouterProduit("Pas ouf");
	TestMag.ajouterProduit("Déjà vu");
	TestMag.ajouterProduit("LIS");
	TestMag.ajouterProduit("Tales of");
	TestMag.ajouterProduit("Magic");
	
	TestMag.ajouterLocation(1,4);
	TestMag.ajouterLocation(3,3);
	TestMag.ajouterLocation(2,5);
	
    CHECK_EQUAL(TestMag.nbLocations(), 3);
}


TEST(GroupMagasin, Magasin_test8)  {
	Magasin TestMag;
	TestMag.ajouterClient("Brunet");
	TestMag.ajouterClient("Cordier");
	TestMag.ajouterClient("Decarpentry");
	TestMag.ajouterClient("Deleglise");
	
	TestMag.ajouterProduit("Pas ouf");
	TestMag.ajouterProduit("Déjà vu");
	TestMag.ajouterProduit("LIS");
	TestMag.ajouterProduit("Tales of");
	TestMag.ajouterProduit("Magic");
	
	TestMag.ajouterLocation(1,4);
	TestMag.ajouterLocation(3,3);
	TestMag.ajouterLocation(2,5);
	TestMag.ajouterLocation(2,5);
	
    CHECK_EQUAL(TestMag.nbLocations(), 3);
}


TEST(GroupMagasin, Magasin_test9)  {
	Magasin TestMag;
	TestMag.ajouterClient("Brunet");
	TestMag.ajouterClient("Cordier");
	TestMag.ajouterClient("Decarpentry");
	TestMag.ajouterClient("Deleglise");
	
	TestMag.ajouterProduit("Pas ouf");
	TestMag.ajouterProduit("Déjà vu");
	TestMag.ajouterProduit("LIS");
	TestMag.ajouterProduit("Tales of");
	TestMag.ajouterProduit("Magic");
	
	TestMag.ajouterLocation(1,4);
	TestMag.ajouterLocation(3,3);
	TestMag.ajouterLocation(2,5);
	TestMag.supprimerLocation(2,5);
	
    CHECK_EQUAL(TestMag.nbLocations(), 2);
}

TEST(GroupMagasin, Magasin_test10)  {
	Magasin TestMag;
	TestMag.ajouterClient("Brunet");
	TestMag.ajouterClient("Cordier");
	TestMag.ajouterClient("Decarpentry");
	TestMag.ajouterClient("Deleglise");
	
	TestMag.ajouterProduit("Pas ouf");
	TestMag.ajouterProduit("Déjà vu");
	TestMag.ajouterProduit("LIS");
	TestMag.ajouterProduit("Tales of");
	TestMag.ajouterProduit("Magic");
	
	TestMag.ajouterLocation(1,4);
	TestMag.ajouterLocation(3,3);
	TestMag.ajouterLocation(2,5);
	TestMag.supprimerLocation(2,5);
	TestMag.supprimerLocation(2,5);
	
    CHECK_EQUAL(TestMag.nbLocations(), 2);
}

TEST(GroupMagasin, Magasin_test11)  {
	Magasin TestMag;
	TestMag.ajouterClient("Brunet");
	TestMag.ajouterClient("Cordier");
	TestMag.ajouterClient("Decarpentry");
	TestMag.ajouterClient("Deleglise");
	
	TestMag.ajouterProduit("Pas ouf");
	TestMag.ajouterProduit("Déjà vu");
	TestMag.ajouterProduit("LIS");
	TestMag.ajouterProduit("Tales of");
	TestMag.ajouterProduit("Magic");
	
	TestMag.ajouterLocation(1,4);
	TestMag.ajouterLocation(3,3);
	TestMag.ajouterLocation(2,5);
	
    CHECK_EQUAL(TestMag.calculerClientsLibres().size(), 1);
}

TEST(GroupMagasin, Magasin_test12)  {
	Magasin TestMag;
	TestMag.ajouterClient("Brunet");
	TestMag.ajouterClient("Cordier");
	TestMag.ajouterClient("Decarpentry");
	TestMag.ajouterClient("Deleglise");
	
	TestMag.ajouterProduit("Pas ouf");
	TestMag.ajouterProduit("Déjà vu");
	TestMag.ajouterProduit("LIS");
	TestMag.ajouterProduit("Tales of");
	TestMag.ajouterProduit("Magic");
	
	TestMag.ajouterLocation(1,4);
	TestMag.ajouterLocation(3,3);
	TestMag.ajouterLocation(2,5);
	
    CHECK_EQUAL(TestMag.calculerProduitsLibres().size(), 2);
}

TEST(GroupMagasin, Magasin_test13)  {
	Magasin TestMag;
	TestMag.ajouterClient("Brunet");
	TestMag.ajouterClient("Cordier");
	TestMag.ajouterClient("Decarpentry");
	TestMag.ajouterClient("Deleglise");
	
	TestMag.ajouterProduit("Pas ouf");
	TestMag.ajouterProduit("Déjà vu");
	TestMag.ajouterProduit("LIS");
	TestMag.ajouterProduit("Tales of");
	TestMag.ajouterProduit("Magic");
	
	TestMag.ajouterLocation(1,4);
	TestMag.ajouterLocation(3,3);
	TestMag.ajouterLocation(2,5);
	
    CHECK_EQUAL(TestMag.trouverProduitDansLocation(3), true);
}

TEST(GroupMagasin, Magasin_test14)  {
	Magasin TestMag;
	TestMag.ajouterClient("Brunet");
	TestMag.ajouterClient("Cordier");
	TestMag.ajouterClient("Decarpentry");
	TestMag.ajouterClient("Deleglise");
	
	TestMag.ajouterProduit("Pas ouf");
	TestMag.ajouterProduit("Déjà vu");
	TestMag.ajouterProduit("LIS");
	TestMag.ajouterProduit("Tales of");
	TestMag.ajouterProduit("Magic");
	
	TestMag.ajouterLocation(1,4);
	TestMag.ajouterLocation(3,3);
	TestMag.ajouterLocation(2,5);
	
    CHECK_EQUAL(TestMag.trouverProduitDansLocation(1), false);
}

TEST(GroupMagasin, Magasin_test15)  {
	Magasin TestMag;
	TestMag.ajouterClient("Brunet");
	TestMag.ajouterClient("Cordier");
	TestMag.ajouterClient("Decarpentry");
	TestMag.ajouterClient("Deleglise");
	
	TestMag.ajouterProduit("Pas ouf");
	TestMag.ajouterProduit("Déjà vu");
	TestMag.ajouterProduit("LIS");
	TestMag.ajouterProduit("Tales of");
	TestMag.ajouterProduit("Magic");
	
	TestMag.ajouterLocation(1,4);
	TestMag.ajouterLocation(3,3);
	TestMag.ajouterLocation(2,5);
	
    CHECK_EQUAL(TestMag.trouverClientDansLocation(1), true);
}

TEST(GroupMagasin, Magasin_test16)  {
	Magasin TestMag;
	TestMag.ajouterClient("Brunet");
	TestMag.ajouterClient("Cordier");
	TestMag.ajouterClient("Decarpentry");
	TestMag.ajouterClient("Deleglise");
	
	TestMag.ajouterProduit("Pas ouf");
	TestMag.ajouterProduit("Déjà vu");
	TestMag.ajouterProduit("LIS");
	TestMag.ajouterProduit("Tales of");
	TestMag.ajouterProduit("Magic");
	
	TestMag.ajouterLocation(1,4);
	TestMag.ajouterLocation(3,3);
	TestMag.ajouterLocation(2,5);
	
    CHECK_EQUAL(TestMag.trouverClientDansLocation(4), false);
}

