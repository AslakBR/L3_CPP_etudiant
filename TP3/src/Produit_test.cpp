#include "Produit.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupProduit) { };

	Produit TestPro(18, "C'est un beau produit");

TEST(GroupProduit, Produit_test1)  {
    CHECK_EQUAL(TestPro.getId(), 18);
}

TEST(GroupProduit, Produit_test2)  {
    CHECK_EQUAL(TestPro.getDescription(), "C'est un beau produit");
}
