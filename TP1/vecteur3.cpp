#include "vecteur3.h"
#include <iostream>
#include <cmath>

void afficher(Vecteur3D *vAfficher){
    std::cout<<"("<<vAfficher->x<<", "<<vAfficher->y<<", "<<vAfficher->z<<")"<<std::endl;
}

void Vecteur3D::afficher() const{
    std::cout<<"("<<x<<", "<<y<<", "<<z<<")"<<std::endl;
}

float Vecteur3D::norme() const{
    return sqrt(pow(x,2)+pow(y,2)+pow(z,2));
}

float Vecteur3D::produitScalaire(Vecteur3D *vScalaire) const{
    return x*vScalaire->x+y*vScalaire->y+z*vScalaire->z;
}

Vecteur3D Vecteur3D::addition(Vecteur3D *vAddition) const{
    Vecteur3D retour{x+vAddition->x,y+vAddition->y,z+vAddition->z};
    return retour;
}





//Il suffit de l'ajouter à l'add Executable du cmakelists
