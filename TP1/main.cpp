#include <cctype>
#include <iostream>
#include "Fibonacci.hpp"
#include "vecteur3.h"

int main(void){
    Vecteur3D vMain;
    vMain.x=2.0;
    vMain.y=3.0;
    vMain.z=6.0;
    vMain.afficher();
    afficher(&vMain);
    std::cout<<vMain.norme()<<std::endl;
    Vecteur3D vScalaire{1.0,2.0,5.0};
    vScalaire.afficher();
    std::cout<<vMain.produitScalaire(&vScalaire)<<std::endl;
    vMain.addition(&vScalaire).afficher();
	return 0; 
}

//g++ -c Fibonacci.cpp
//g++ -c main.cpp
//g++ Fibonacci.o main.o

//qtcreator indique l'emplacement des erreurs
