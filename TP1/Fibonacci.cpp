#include <cctype>
#include <iostream>
#include "Fibonacci.hpp"

int fibonacciRecursif(int n){
	if(n==0)
		return 0;
	if(n==1)
		return 1;
	return fibonacciRecursif(n-1)+fibonacciRecursif(n-2);
}

int fibonacciIteratif(int n){
	int tmp;
	int val0=0,val1=1;
	for(int i=1; i<=n;i++){
		tmp=val1;
		val1+=val0;
		val0=tmp;
	}
	return val0;
}
