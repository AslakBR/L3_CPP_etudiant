#ifndef VECTEUR3_H
#define VECTEUR3_H

struct Vecteur3D {
    float x,y,z;
    void afficher() const;
    float norme() const;
    float produitScalaire(Vecteur3D *vScalaire) const;
    Vecteur3D addition(Vecteur3D *vAddition) const;
};

void afficher(Vecteur3D *vAfficher);
#endif // VECTEUR3_H
