#include "Fibonacci.hpp"
#include <CppUTest/CommandLineTestRunner.h>
TEST_GROUP(GroupFibonacci) { };

TEST(GroupFibonacci, test_FiboIt_0) { // premier test unitaire
    int result = fibonacciIteratif(0);
    CHECK_EQUAL(0, result);
}

TEST(GroupFibonacci, test_FiboIt_1) { // Deuxieme test unitaire
    int result = fibonacciIteratif(1);
    CHECK_EQUAL(1, result);
}

TEST(GroupFibonacci, test_FiboIt_2) { // troisieme test unitaire
    int result = fibonacciIteratif(2);
    CHECK_EQUAL(1, result);
}

TEST(GroupFibonacci, test_FiboIt_3) { // quatrieme test unitaire
    int result = fibonacciIteratif(3);
    CHECK_EQUAL(2, result);
}

TEST(GroupFibonacci, test_FiboIt_4) { // cinquieme test unitaire
    int result = fibonacciIteratif(4);
    CHECK_EQUAL(3, result);
}

TEST(GroupFibonacci, test_FiboIt_5) { // sixieme test unitaire
    int result = fibonacciIteratif(5);
    CHECK_EQUAL(5, result);
}


TEST(GroupFibonacci, test_FiboRe_0) { // premier test unitaire
    int result = fibonacciRecursif(0);
    CHECK_EQUAL(0, result);
}

TEST(GroupFibonacci, test_FiboRe_1) { // deuxieme test unitaire
    int result = fibonacciRecursif(1);
    CHECK_EQUAL(1, result);
}

TEST(GroupFibonacci, test_FiboRe_2) { // troisieme test unitaire
    int result = fibonacciRecursif(2);
    CHECK_EQUAL(1, result);
}

TEST(GroupFibonacci, test_FiboRe_3) { // quatrieme test unitaire
    int result = fibonacciRecursif(3);
    CHECK_EQUAL(2, result);
}

TEST(GroupFibonacci, test_FiboRe_4) { // cinquieme test unitaire
    int result = fibonacciRecursif(4);
    CHECK_EQUAL(3, result);
}

TEST(GroupFibonacci, test_FiboRe_5) { // sixieme test unitaire
    int result = fibonacciRecursif(5);
    CHECK_EQUAL(5, result);
}
